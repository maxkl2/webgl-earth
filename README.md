# WegGL Earth

A three-dimensional model of planet Earth, built with WebGL.
The diffuse and normal maps are quite big, so loading takes a while.

To view locally, serve the `public/` directory with any web server and open `index.html` in your browser.
